package ru.samborskiy.information.search.searcher.request;

import ru.samborskiy.information.search.entity.InvertedIndex;

import java.util.Set;

public class Or extends OperatorNode {

    public Or(RequestNode left, RequestNode right) {
        super(left, right);
    }

    @Override
    public Set<Integer> apply(InvertedIndex invertedIndex) {
        Set<Integer> left = getLeft(invertedIndex);
        Set<Integer> right = getRight(invertedIndex);
        for (Integer file : left) {
            right.add(file);
        }
        return right;
    }
}
