package ru.samborskiy.information.search.searcher.request;

import ru.samborskiy.information.search.entity.InvertedIndex;

import java.util.Set;

public interface RequestNode {

    public Set<Integer> apply(InvertedIndex invertedIndex);
}
