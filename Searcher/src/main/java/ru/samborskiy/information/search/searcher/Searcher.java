package ru.samborskiy.information.search.searcher;

import ru.samborskiy.information.search.entity.InvertedIndex;

import java.io.File;
import java.util.List;

public class Searcher {

    private final static String USAGE = "Usage: <request> [-f index file]";
    private final static String INDEX_FILE_FLAG = "-f";
    private final static String INDEX_FILE_DEFAULT = "index_file";

    public static void main(String[] args) {
        switch (args.length) {
            case 1:
                search(args[0], INDEX_FILE_DEFAULT);
                break;
            case 3:
                if (args[1].equals(INDEX_FILE_FLAG)) {
                    search(args[0], args[2]);
                    break;
                }
            default:
                System.out.println(USAGE);
                break;
        }
    }

    private static void search(String requestString, String indexFile) {
        try {
            InvertedIndex invertedIndex = InvertedIndex.read(new File(indexFile));
            SearchBuilder searchBuilder = new SearchBuilder(invertedIndex);
            List<String> result = searchBuilder.make(requestString);
            for (String res : result) {
                System.out.println(res);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
