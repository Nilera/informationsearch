package ru.samborskiy.information.search.searcher.request;

import ru.samborskiy.information.search.entity.InvertedIndex;

import java.util.HashSet;
import java.util.Set;

public class And extends OperatorNode {

    public And(RequestNode left, RequestNode right) {
        super(left, right);
    }

    @Override
    public Set<Integer> apply(InvertedIndex invertedIndex) {
        Set<Integer> left = getLeft(invertedIndex);
        Set<Integer> right = getRight(invertedIndex);
        Set<Integer> result = new HashSet<>();
        for (Integer file : left) {
            if (right.contains(file)) {
                result.add(file);
            }
        }
        return result;
    }
}
