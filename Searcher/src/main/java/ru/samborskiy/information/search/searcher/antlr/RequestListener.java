// Generated from E:/Java/InformationSearch/Searcher\Request.g4 by ANTLR 4.5
package ru.samborskiy.information.search.searcher.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RequestParser}.
 */
public interface RequestListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RequestParser#or}.
	 * @param ctx the parse tree
	 */
	void enterOr(@NotNull RequestParser.OrContext ctx);
	/**
	 * Exit a parse tree produced by {@link RequestParser#or}.
	 * @param ctx the parse tree
	 */
	void exitOr(@NotNull RequestParser.OrContext ctx);
	/**
	 * Enter a parse tree produced by {@link RequestParser#and}.
	 * @param ctx the parse tree
	 */
	void enterAnd(@NotNull RequestParser.AndContext ctx);
	/**
	 * Exit a parse tree produced by {@link RequestParser#and}.
	 * @param ctx the parse tree
	 */
	void exitAnd(@NotNull RequestParser.AndContext ctx);
	/**
	 * Enter a parse tree produced by {@link RequestParser#bracket}.
	 * @param ctx the parse tree
	 */
	void enterBracket(@NotNull RequestParser.BracketContext ctx);
	/**
	 * Exit a parse tree produced by {@link RequestParser#bracket}.
	 * @param ctx the parse tree
	 */
	void exitBracket(@NotNull RequestParser.BracketContext ctx);
}