package ru.samborskiy.information.search.searcher.request;

import ru.samborskiy.information.search.entity.InvertedIndex;

import java.util.Set;

public abstract class OperatorNode implements RequestNode {

    private final RequestNode left;
    private final RequestNode right;

    public OperatorNode(RequestNode left, RequestNode right) {
        this.left = left;
        this.right = right;
    }

    protected Set<Integer> getLeft(InvertedIndex invertedIndex) {
        return left.apply(invertedIndex);
    }

    protected Set<Integer> getRight(InvertedIndex invertedIndex) {
        return right.apply(invertedIndex);
    }

    @Override
    public abstract Set<Integer> apply(InvertedIndex invertedIndex);
}
