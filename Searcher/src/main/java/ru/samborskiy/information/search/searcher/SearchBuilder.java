package ru.samborskiy.information.search.searcher;

import ru.samborskiy.information.search.searcher.request.Parser;
import ru.samborskiy.information.search.searcher.request.RequestNode;
import ru.samborskiy.information.search.entity.InvertedIndex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class SearchBuilder {

    private static final String REQUEST_FORM = "%d. %s\n--- %s\n";
    private static final String NO_RESULT = "Search returned no results";

    private final InvertedIndex invertedIndex;

    public SearchBuilder(InvertedIndex invertedIndex) {
        this.invertedIndex = invertedIndex;
    }

    public List<String> make(String request) throws IOException {
        RequestNode requestNode = Parser.parse(request);
        Set<Integer> files = requestNode.apply(invertedIndex);
        List<String> result = new ArrayList<>();
        if (files != null) {
            int index = 1;
            for (Integer file : files) {
                result.add(String.format(REQUEST_FORM, index, invertedIndex.getFile(file), invertedIndex.getTitle(file)));
                index++;
            }
        } else {
            result.add(NO_RESULT);
        }
        return result;
    }

}
