package ru.samborskiy.information.search.searcher.request;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import ru.samborskiy.information.search.searcher.antlr.RequestLexer;
import ru.samborskiy.information.search.searcher.antlr.RequestParser;

import java.io.IOException;

public class Parser {

    private Parser() {
    }

    public static RequestNode parse(String request) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(request);
        RequestLexer lexer = new RequestLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        RequestParser parser = new RequestParser(tokens);
        RuleContext tree = parser.or();
        return parseTree(tree);
    }

    private static RequestNode parseTree(ParseTree tree) {
        if (tree.getChildCount() == 0) {
            return new Word(tree.getText());
        }
        if (tree.getChildCount() == 1) {
            return parseTree(tree.getChild(0));
        }
        if (tree instanceof RequestParser.OrContext) {
            return new Or(parseTree(tree.getChild(0)), parseTree(tree.getChild(2)));
        } else if (tree instanceof RequestParser.AndContext) {
            return new And(parseTree(tree.getChild(0)), parseTree(tree.getChild(2)));
        } else {
            return parseTree(tree.getChild(1));
        }
    }

}
