package ru.samborskiy.information.search.searcher.request;

import ru.samborskiy.information.search.entity.InvertedIndex;

import java.util.Set;

public class Word implements RequestNode {

    private final String word;

    public Word(String word) {
        this.word = word;
    }

    @Override
    public Set<Integer> apply(InvertedIndex invertedIndex) {
        return invertedIndex.getFiles(word);
    }
}
