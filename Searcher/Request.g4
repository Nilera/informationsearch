grammar Request;

or
    : or '|' and
    | and
    ;

and
    : and '&' bracket
    | bracket
    ;

bracket
    : '(' or ')'
    | STRING
    ;

STRING   : [a-zA-Z]+ ;
WS       : [ \t\r\n\u000C]+ -> skip ;