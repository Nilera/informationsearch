package ru.samborskiy.information.search.entity;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class InvertedIndex {

    private static final String TITLES = "titles";
    private static final String INDEXES = "indexes";
    private static final String WORD = "w";
    private static final String PATH = "p";
    private static final String TITLE = "t";

    private final List<String> files;
    private final List<String> titles;
    private final Map<String, Set<Integer>> indexes;

    public InvertedIndex() {
        this.files = new ArrayList<>();
        this.titles = new ArrayList<>();
        this.indexes = new HashMap<>();
    }

    public int size() {
        return indexes.size();
    }

    public void addFile(String filePath) {
        files.add(filePath);
    }

    public void addTitle(String title) {
        titles.add(title);
    }

    public void addIndex(String word, Set<Integer> fileIds) {
        indexes.put(word, fileIds);
    }

    public void addTokens(Integer documentId, Set<String> tokens) {
        for (String token : tokens) {
            if (indexes.containsKey(token)) {
                Set<Integer> files = indexes.get(token);
                files.add(documentId);
            } else {
                Set<Integer> files = new HashSet<>();
                files.add(documentId);
                indexes.put(token, files);
            }
        }
    }

    public String getTitle(Integer fileId) {
        return titles.get(fileId);
    }

    public String getFile(Integer fileId) {
        return files.get(fileId);
    }

    public Set<Integer> getFiles(String word) {
        return indexes.get(word.toLowerCase());
    }

    public void write(File file) throws IOException {
        JsonFactory factory = new JsonFactory();
        JsonGenerator generator = factory.createGenerator(file, JsonEncoding.UTF8);
        generator.writeStartObject();
        writeTitles(generator);
        writeIndexes(generator);
        generator.writeEndObject();
        generator.close();
    }

    private void writeTitles(JsonGenerator generator) throws IOException {
        generator.writeArrayFieldStart(TITLES);
        for (int i = 0; i < titles.size(); i++) {
            writeTitle(generator, titles.get(i), files.get(i));
        }
        generator.writeEndArray();
    }

    private void writeTitle(JsonGenerator generator, String title, String file) throws IOException {
        generator.writeStartObject();
        generator.writeStringField(PATH, file);
        generator.writeStringField(TITLE, title);
        generator.writeEndObject();
    }

    private void writeIndexes(JsonGenerator generator) throws IOException {
        generator.writeArrayFieldStart(INDEXES);
        for (Entry<String, Set<Integer>> index : indexes.entrySet()) {
            writeIndex(generator, index);
        }
        generator.writeEndArray();
    }

    private void writeIndex(JsonGenerator generator, Map.Entry<String, Set<Integer>> index) throws IOException {
        generator.writeStartObject();
        generator.writeStringField(WORD, index.getKey());
        generator.writeArrayFieldStart(PATH);
        writeList(generator, index.getValue());
        generator.writeEndArray();
        generator.writeEndObject();
    }

    private void writeList(JsonGenerator generator, Set<Integer> values) throws IOException {
        for (Integer value : values) {
            generator.writeNumber(value);
        }
    }

    public static InvertedIndex read(File file) throws IOException {
        InvertedIndex invertedIndex = new InvertedIndex();
        JsonFactory factory = new JsonFactory();
        JsonParser parser = factory.createParser(file);
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            String field = parser.getCurrentName();
            if (TITLES.equals(field)) {
                readTitles(parser, invertedIndex);
            } else if (INDEXES.equals(field)) {
                readIndexes(parser, invertedIndex);
            }
        }
        return invertedIndex;
    }

    private static void readTitles(JsonParser parser, InvertedIndex invertedIndex) throws IOException {
        parser.nextToken();
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            readTitle(parser, invertedIndex);
        }
    }

    private static void readTitle(JsonParser parser, InvertedIndex invertedIndex) throws IOException {
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            String field = parser.getCurrentName();
            if (PATH.equals(field)) {
                parser.nextToken();
                invertedIndex.addFile(parser.getText());
            } else if (TITLE.equals(field)) {
                parser.nextToken();
                invertedIndex.addTitle(parser.getText());
            }
        }
    }

    private static void readIndexes(JsonParser parser, InvertedIndex invertedIndex) throws IOException {
        parser.nextToken();
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            readIndex(parser, invertedIndex);
        }
    }

    private static void readIndex(JsonParser parser, InvertedIndex invertedIndex) throws IOException {
        String word = null;
        Set<Integer> paths = null;
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            String field = parser.getCurrentName();
            if (WORD.equals(field)) {
                parser.nextToken();
                word = parser.getText();
            } else if (PATH.equals(field)) {
                paths = readList(parser);
            }
        }
        invertedIndex.addIndex(word, paths);
    }

    private static Set<Integer> readList(JsonParser parser) throws IOException {
        Set<Integer> list = new HashSet<>();
        parser.nextToken();
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            list.add(parser.getIntValue()); // TODO ?
        }
        return list;
    }
}
