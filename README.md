## Build
**REQUIRED Java 7**

To build the project you must call:
```
#!bash
./gradlew build
```
Gradle build 2 fatJar: *Indexer-1.0.jar* and *Searcher-1.0.jar*. Which will be located in directories:
```
#!bash
Indexer/build/libs
```
and
```
#!bash
Searcher/build/libs
```

## Indexer
To run *Indexer* you must call:
```
#!bash
java -jar Indexer-1.0.jar <corpus_directory> [-f index_file]
```
**corpus_directory** - directory where is located corpus for training

**index_file** - file which is stored inverted index (default path of index file is *index_file*).

## Searcher
To run *Searcher* you must call:
```
#!bash
java -jar Searcher-1.0.jar <request> [-f index_file]
```
**request** - request that will be searched

**index_file** - path where is located inverted index file (default path of index file is *index_file*).

Request supports operators **&** and **|** (eg "October & test", "october | test").