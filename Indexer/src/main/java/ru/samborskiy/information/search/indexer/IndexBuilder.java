package ru.samborskiy.information.search.indexer;

import org.xml.sax.SAXException;
import ru.samborskiy.information.search.entity.InvertedIndex;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class IndexBuilder {

    private final InvertedIndex invertedIndex;
    private int tokensNumber;
    private int documentIndex;

    public IndexBuilder() {
        this.invertedIndex = new InvertedIndex();
        this.tokensNumber = 0;
        this.documentIndex = 0;
    }

    public void saveIndex(File file) throws IOException {
        invertedIndex.write(file);
    }

    public int tokensNumber() {
        return tokensNumber;
    }

    public int termNumber() {
        return invertedIndex.size();
    }

    public void build(String dir) throws IOException, SAXException, ParserConfigurationException {
        build(new File(dir));
    }

    public void build(File dir) throws ParserConfigurationException, SAXException, IOException {
        if (dir.exists()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        build(file);
                    } else {
                        parse(file);
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("Corpus directory not found");
        }
    }

    private void parse(File file) throws IOException, SAXException, ParserConfigurationException {
        CorpusFileParser parser = new CorpusFileParser();
        parser.parse(file);
        String filePath = file.getAbsolutePath();
        invertedIndex.addFile(filePath);
        invertedIndex.addTitle(parser.getTitle());
        invertedIndex.addTokens(documentIndex, parser.getTokens());
        tokensNumber += parser.getTokens().size();
        documentIndex++;
    }
}
