package ru.samborskiy.information.search.indexer;

import java.io.File;

public class Indexer {

    private static final String USAGE = "Usage: <corpus directory> [-f index file]";
    private final static String INDEX_FILE_FLAG = "-f";
    private final static String INDEX_FILE_DEFAULT = "index_file";

    public static void main(String[] args) {
        switch (args.length) {
            case 1:
                makeIndex(args[0], INDEX_FILE_DEFAULT);
                break;
            case 3:
                if (args[1].equals(INDEX_FILE_FLAG)) {
                    makeIndex(args[0], args[2]);
                    break;
                }
            default:
                System.out.println(USAGE);
                break;
        }
    }

    private static void makeIndex(String corpusDirectory, String indexFile) {
        try {
            IndexBuilder builder = new IndexBuilder();
            builder.build(corpusDirectory);
            builder.saveIndex(new File(indexFile));
            System.out.format("Tokens: %d\nTerms: %d\n", builder.tokensNumber(), builder.termNumber());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
