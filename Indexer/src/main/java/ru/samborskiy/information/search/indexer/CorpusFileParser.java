package ru.samborskiy.information.search.indexer;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class CorpusFileParser extends DefaultHandler {

    private static final String BODY = "body";
    private static final String TITLE = "title";
    private static final String DELIM = " \n\r\t.,!?;\":()[]=";

    private boolean isBody;
    private boolean isTitle;
    private Set<String> tokens;
    private String title = "";

    public CorpusFileParser() {
        this.isBody = false;
        this.tokens = new HashSet<>();
    }

    public void parse(File file) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(file, this);
    }

    public Set<String> getTokens() {
        return tokens;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        switch (qName) {
            case BODY:
                isBody = true;
                break;
            case TITLE:
                isTitle = true;
                break;
            default:
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        switch (qName) {
            case BODY:
                isBody = false;
                break;
            case TITLE:
                isTitle = false;
                break;
            default:
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        if (isBody) {
            StringTokenizer st = new StringTokenizer(new String(ch, start, length), DELIM);
            while (st.hasMoreTokens()) {
                tokens.add(st.nextToken().toLowerCase());
            }
        }
        if (isTitle) {
            title += new String(ch, start, length);
        }
    }
}
